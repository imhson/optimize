public class Task {
    String name;
    double time;
    double value;
    double g;

    public Task(String n, double t, double v) {
        this.name = n;
        this.time = t;
        this.value = v;
        this.g = v / t;
    }

    public Task() {
        this.g = 0;
    }
}