import java.util.Scanner;
import java.util.ArrayList;

public class Optimize {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double totalTime = 0.0;
        int totalTasks = 0;
        boolean completed = false;
        ArrayList<Task> tasks = new ArrayList<Task>();
        System.out.print("Tổng thời gian bạn có(phút): ");
        totalTime = scanner.nextInt();
        System.out.print("Số nhiệm vụ cần hoàn thành: ");
        totalTasks = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < totalTasks; i++) {
            System.out.println("Nhiệm vụ thứ " + (i + 1));
            System.out.print("Tên: ");
            String name = scanner.nextLine();
            System.out.print("Thời gian cần để hoàn thành: ");
            double time = scanner.nextDouble();
            System.out.print("Giá trị: ");
            double value = scanner.nextDouble();
            tasks.add(new Task(name, time, value));
        }
        System.out.println("Các nhiệm vụ nên hoàn thành:");
        while (!completed) {
            completed = true;
            Task pickedTask = new Task();
            int index = 0;
            for (int i = 0; i < tasks.size(); i++) {
                if (tasks.get(i).g > pickedTask.g && totalTime - tasks.get(i).time >= 0) {
                    pickedTask = tasks.get(i);
                    index = i;
                    completed = false;
                }
            }
            if (!completed) {
                totalTime = totalTime - pickedTask.time;
                System.out.println("-" + pickedTask.name);
                tasks.remove(index);
            }
        }
        scanner.close();
    }
}
